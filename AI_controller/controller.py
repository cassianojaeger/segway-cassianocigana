# -*- coding: utf-8 -*-
from __future__ import division
import numpy
from .state import State
import random
import datetime, time
import math
import sys
from random import randint
import operator

MAX_VARIATIONS_HILL = 30 #30 #QUANTIDADE DE "VIZINHOS" DO ESTADO ATUAL 
TEMPERATURE_ITERATIONS = 999 #QUANTIDADE DE ITERAÇÕES SOBRE UMA TEMPERATURA
VARIATION_HILL = 3.0 #10 #VARIAÇÃO ALEATORIAS DOS VALORES DOS PARAMETROS
MAX_DEVIATION_SAMPLES = 10 #15 #AMOSTRAS DE PERFORMANCE DO MESMO PARAMETRO

class Controller:
    def __init__(self, game, load, state):
        self.initialize_parameters(game, load, state)
        self.initializeCommonVars()
        self.initializeVarHillClimb()
        #self.initializeVarSimulatedAnnealing()
        
    def initialize_parameters(self, game, load,state):
        self.state = state
      
        if load == None:
            self.parameters = numpy.random.normal(0, 1, 2*len(self.compute_features(state))+1)
        else:
            params = open(load, 'r')
            weights = params.read().split("\n")
            self.parameters = [float(x.strip()) for x in weights[:]]

    def initializeCommonVars(self):
        self.bestParameters = 0
        self.bestPerformance = 0

    def initializeVarHillClimb(self):
        self.listOfPerformance = list()
        self.listOfParameters  = list()
        self.listAverage = list()
        self.index = 0
        self.averageSamples = 0
        self.gerarParametros = True
     
    def initializeVarSimulatedAnnealing(self):
        self.Temperature = 1
        self.TemperatureMin = 0.1
        self.alpha = 0.5
        self.oldParameters = list()
        self.oldPerformance = 0
        self.newParameters = list()
        self.newPerformance = 0
        self.isDeviationGenerated = False
        self.indexSA = 0

    def output(self, episode, performance):
       #print "Performance do episodio #%d: %d" % (episode, performance)
       #print self.parameters
       #print self.listOfPerformance
       if episode > 0 and episode % 10 == 0:
           output = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")
           for parameter in self.parameters:
               output.write(str(parameter) + "\n")

#--------------------------------------------------------------------------------------------------------

    #FUNCAO A SER COMPLETADA. Deve utilizar os pesos para calcular as funções de preferência Q para cada ação e retorna
    #-1 caso a ação desejada seja esquerda, +1 caso seja direita, e 0 caso seja ação nula
    def take_action(self, state):
        features = self.compute_features(state)
        #stE = Θ0 + Θ1α².α'² + Θ2x.x²   
        stE = ( self.parameters[0]+self.parameters[1]*features[0]*features[1] 
               +self.parameters[2]*features[2]*features[3])
        #stD = Θ3 + Θ4α².α'² + Θ5x.x² 
        stD = ( self.parameters[3]+self.parameters[4]*features[0]*features[1] 
               +self.parameters[5]*features[2]*features[3])
        #st0 = Θ6 + Θ7α².α'² + Θ8x.x²' 
        st0 = ( self.parameters[6]+self.parameters[7]*features[0]*features[1] 
               +self.parameters[8]*features[2]*features[3])
        #verifica se o movimento mais apropriado é para a esquerda
        #print "velocidade angular: ",state.angular_velocity
        if stE > stD and stE > st0:
            return -1
        #verifica se o movimento mais apropriado é para a direita
        if stD > stE and stD > st0:
            return 1

        return 0

    #FUNCAO A SER COMPLETADA. Deve calcular features expandidas do estados (Dica: deve retornar um vetor)
    def compute_features(self,state):
        # feature1.0 
        #features = [state.rod_angle**2,state.angular_velocity,state.wheel_x/1200,state.wheel_x**2/1200**2]
        #features2.0 
        features = [(state.rod_angle-180)/(180+180),(state.angular_velocity-360)/(360+360),state.wheel_x/1200,state.wheel_x**2/1200**2]
        #features2.1 
        #features = [(state.rod_angle-180)/(180+180),(state.angular_velocity-360)/(360+360),state.wheel_x/1000,state.wheel_x**2/1000**2]
        #features2.2 
        #features = [(state.rod_angle-180)/(180+180),(state.angular_velocity-500)/(500+500),state.wheel_x/1100,state.wheel_x**2/1100**2]
        return features

    #FUNCAO A SER COMPLETADA. Deve atualizar a propriedade self.parameters
    def update(self, episode, performance):
        #nossa versão do algoritmo Hill Climbing

        #TODO É preciso arrumar a geração de vizinhanças do hill climbing:
        #Gerar arrays de numeros aleatorios e somar com o parametro atual
        #fazer isso um numero x de vezes
        self.myHillClimbing(performance)
        #self.mySimulatedAnnealing(performance)
        return self.parameters

#--------------------------------------------------------------------------------------------------------
#FUNÇÕES AUXILIARES NO PROCESSO DE APRENDIZADO DO CONTROLADOR ATRAVÉS DE SIMULATED ANNEALING
    def mySimulatedAnnealing(self,performance):
        #modifica a temperatura dependendo da quantidade de iterações feitas 
        if self.indexSA == TEMPERATURE_ITERATIONS:
            self.Temperature = self.alpha*self.Temperature
            self.indexSA = 1
        #caso a temperatura chegue a um valor menor que o mínimo permetido, o método acaba
        if self.Temperature < self.TemperatureMin:
            output = open("./finalparams/learntParamsSA.txt", "w+")
            for parameter in self.parameters:
                output.write(str(parameter) + "\n")

            output = open("./finalparams/bestParamSA.txt", "w+")
            for parameter in self.parameters:
                output.write(str(parameter) + "\n")
            sys.exit(1)

        #se ainda não geramos um vizinho aleatorio
        if not self.isDeviationGenerated:
            self.oldParameters = list(self.parameters)
            self.oldPerformance = performance
            self.parameters = self.generateParametersDeviationsSimulatedAnnealing()
            self.isDeviationGenerated = True
        else: #se já foi gerado um vizinho, nós agora fazemos a escolha de qual parametro é melhor
            self.newParameters = list(self.parameters)
            self.newPerformance = performance
            ap = self.acceptanceProbability() #função de probabilidade de aceitação da resposta "Pior"
            if ap>random.uniform(0.0, 1.0):#se a nova for melhor, corrige os parametros
                self.parameters = self.newParameters
                self.oldPerformance = self.newPerformance 
            else: #senão, os antigos são melhores ainda
                self.parameters = self.oldParameters
                self.oldPerformance = self.oldPerformance 
            self.isDeviationGenerated = False
            self.indexSA += 1
            if self.bestPerformance < self.oldPerformance: #salva os parametros que geraram o maior Valor(s)
                self.bestParameters = list(self.parameters)     

    #TODO Criar mais vizinhos e fazer a comparação com todos
    def generateParametersDeviationsSimulatedAnnealing(self):
        deviationList = list(self.parameters)
        deviationList = list(random.sample(range(-self.variation, self.variation), len(self.parameters)))
        return deviationList

    def acceptanceProbability(self):
        new = -self.newPerformance
        old = -self.oldPerformance
        if new < old:
            return 1
        else:
            try:
                return math.exp((old - new/self.Temperature))
            except OverflowError:
                return 0


#--------------------------------------------------------------------------------------------------------
#FUNÇÕES AUXILIARES NO PROCESSO DE APRENDIZADO DO CONTROLADOR ATRAVÉS DE HILL CLIMBING

    #função que vai gerar os desvios de cada componente theta    
    def generateParametersDeviationsHillClimbing(self):
        #insere os parametros ótimos na posição inicial da lista de parametros variados
        list_aux = list()
        list_aux.insert(0,self.parameters) 
       
        #gera parametros com variações aleatórias restringidas por self.variation em cada valor de theta
        #TODO Ao invéz de fazer um aleatorio para cada [index]+x e [index]-x
        #criar um array de aleatorios e somar aos parametros
        for x in range(0,MAX_VARIATIONS_HILL):
            aux1 = [random.uniform(-VARIATION_HILL,VARIATION_HILL) for _ in range(len(self.parameters))]
            list_aux.append(list(map(operator.add, aux1, self.parameters)))

        self.gerarParametros = False
        return list_aux #retorna lista com MAX_VARIATIONS_HILL vizinhos aleatorios do parametro atual

    #descobre quais são os melhores parametros entre os vizinhos gerados    
    def discoverBestParameters(self):
        maxPerformance = -1
        performanceIndex = 0
        for valor in range(0,len(self.listOfPerformance)-1):
            if maxPerformance < self.listOfPerformance[valor]: 
                maxPerformance = self.listOfPerformance[valor]
                performanceIndex = valor

        if self.bestPerformance < self.listOfPerformance[performanceIndex]:
            self.bestParameters = self.listOfParameters[performanceIndex]
            self.bestPerformance = self.listOfPerformance[performanceIndex]

        print "PERFORMANCE DO MELHOR EPISODIO ESCOLHIDO: ",self.listOfPerformance[performanceIndex]
        #print "PARAMETROS DO MELHOR EP: ",self.listOfParameters[performanceIndex]        
        return self.listOfParameters[performanceIndex]

    def myHillClimbing(self,performance):
        #se estivermos começando a avaliação dos possiveis estados, então: 
        if self.noDeviationsGenerated():
            self.listOfParameters = list(self.generateParametersDeviationsHillClimbing())#criar o array com as variações de self.parameters atual
            self.listOfPerformance = list()
            self.gerarParametros = False

        if not self.averagePerformanceDone():
            #print self.parameters
            self.listAverage.insert(self.averageSamples, performance)
        else:
            meanValue = float(sum(self.listAverage)/(len(self.listAverage)))
            self.listOfPerformance.insert(self.index, meanValue)#lista que recebe a função Valor(self.performance) do episodio anterior  
            self.parameters = list(self.listOfParameters[self.index+1])#alimenta os parametros com o próximo desvio calculado anteriormente 
            self.listAverage = list()
            #print "parametros : ",self.parameters
            if meanValue >= self.bestPerformance:
                print "performance media do parametro avaliado: ",meanValue
                output = open("./finalPerformance/listOfPerformance.txt" , "a")
                output.write(str(meanValue) + "\n")


        if self.endOfDeviationSimulation():
            best_parameters = self.discoverBestParameters()#fazer a escolha dos parametros que deram o maior Valor(s)
            
            if best_parameters is self.listOfParameters[0]:
                #continua com o valor anterior, pois não achamos nenhuma
                #configuração de parametros que de um valor maior na função Valor(s) usada anteriormente    
                self.endLearning() 
            else:
                #foi achado uma variação de parametros com Valor(s) maior que os parametros sem a variação
                self.parameters = list(best_parameters)
                self.gerarParametros = True
            self.index = 0 #reinicia o index      
        else:
            if self.averagePerformanceDone():
                self.index += 1
                self.averageSamples = 0
            else:
                self.averageSamples += 1
        

    #func aux que verifica se terminaram as simulações para as variações dos parametros originais
    def endOfDeviationSimulation(self):
        if self.index == (len(self.listOfParameters)-1):
            return True
        else:
            return False
    #func aux que verifica se foram geradas as variações para o parametro escolhido
    def noDeviationsGenerated(self):
        if self.gerarParametros:
            return True
        else:
            return False

    def endLearning(self):
        output = open("./finalparams/learntParamsHC.txt" , "w+")
        for parameter in self.parameters:
            output.write(str(parameter) + "\n")

        output = open("./finalparams/bestPerformanceParamsHC.txt" , "w+")
        for parameter in self.bestParameters:
            output.write(str(parameter) + "\n")  
        sys.exit(1)

    def averagePerformanceDone(self):
        if self.averageSamples == MAX_DEVIATION_SAMPLES:
            return True
        else:
            return False